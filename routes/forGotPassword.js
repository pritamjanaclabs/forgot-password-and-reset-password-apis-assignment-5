/**
 * Created by karan on 3/9/2015.
 */
var express = require('express');
var router = express.Router();
var commonFunc=require('./commonFunction');
var async = require('async');
var md5 = require('MD5');
var sendResponse = require('./sendResponse');


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('forgotPassword');
});

//get API for forgot mail id and also send a mail to mail id
router.post('/forgot_password', function(req, res, next) {
    var email = req.body.email;
    var values = [email];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, values, callback);
        },function (callback) {
            checkExist(res,email,callback);
        }],function(resultCallback){
            var sub ="Reset Password";
            var port=config.get('PORT');
            var message ='<a href="http://localhost:'+port+'/reset_password?token='+resultCallback[0].accessToken+'">Reset your password</a>';
            commonFunc.sendEmail(email, sub, message, function (result) {
                if (result == 1) {
                    var data="mail has been sent to reset your password";
                    sendResponse.sendSuccessData(data,res);
                }
                else{
                    var data="Sending mail error please try again";
                    sendResponse.sendErrorMessage(data,res);
                }
            });
        }
    );
});

//get reset password from mail id
router.get('/reset_password', function(req, res, next) {
    res.sendfile('./views/resetPassword.html');
});

//check access token and also change password
router.post('/setPassword', function(req, res, next) {
    var accessToken = req.body.accessToken;
    var newPassword = req.body.password;
    var values = [accessToken, newPassword];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, values, callback);
        }], function (callbackResult) {
        var sql = "SELECT `userId`,`uMail` FROM `userinfo` WHERE `accessToken`=? LIMIT 1";
        dbConnection.Query(res, sql, accessToken, function (result) {
            if (result.length == 0) {
                var data="Link has been expired";
                sendResponse.sendErrorMessage(data,res);
            }
            else {
                var userId = result[0].userId;
                var userMail = result[0].uMail;
                var encryptPassword = md5(newPassword);
                var loginTime = new Date();
                var newAccessToken = md5(userMail + newPassword + loginTime);
                var sql = "UPDATE `userinfo` SET `uPass`=?,`accessToken`=? WHERE `userId`=? LIMIT 1";
                dbConnection.Query(res, sql, [encryptPassword, newAccessToken, userId], function (userResponse2) {
                    var data="Successfully change your password";
                    sendResponse.sendSuccessData(data,res);
                });

            }
        });

    });

});

//check mail id is it exsit or not
function checkExist(res,email,callback) {
    var sql = "SELECT `uName`,`accessToken`,`uPass` FROM `userinfo` WHERE `uMail`=? limit 1";
    dbConnection.Query(res, sql, email, function (userResponse) {
        if (userResponse.length == 0) {
            sendResponse.wrongMailId(res);
        }
        else{
            return callback(userResponse);
        }
    });
}
module.exports = router;
